const req = name =>
  {
    throw new Error(`[${name}] is a required parameter`)
  }

// Pseudo code to generate a random session id string
const sessionUUID = ( length = 10 ) =>
  Array.from({ length })
    .map
      ( _ =>
          ( ~~( Math.random()*36 ) )
            .toString(36)
      ).join('')

// Finds the first item in the list that can fit within the limit, reduces the limit total by the ammount added and then recurses
// Could potentially abstract
const deliveryWeightPack = ( items, limit, packed = [] ) =>
  {
    for (const [ index, item ] of items.entries())
      if ( item.weight <= limit )
        return items.splice(index, 1) &&
          deliveryWeightPack
            ( items 
            , limit - item.weight
            , [ ...packed
              , item
              ]
            )

    return packed
  }

export default class {
  constructor
    ( db = req('db')
    , session
    , weightLimit = 10
    ) {
      this._db = db

      this._session = session

      this._limit = weightLimit

      this._products = []

      this._state = this.init()
    }
  
  get products  () {
    return this._products
  }

  get state  () {
    return this._state
  }

  // Retreives any existing basket products if available
  async init () {
    // If no session ID is provided, generate one and  flag as a new session
    if (!this._session) {
      this._session = sessionUUID()

      return 'new'
    }

    try {
      const items = await this._db.getBasketItems( this._session )

      await Promise.all
        (
          items.map
            ( item => 
                this.add( item )
            )
        )

      // If a session is found and restored without fail
      return 'restored'

    } catch (e) {

      // TODO - Implement an error handling system the queries error codes
      switch (e.message) {
        // State indicates that the current basket is new and not existing
        case 'Session not found':
          return 'lost'
        default:
          return 'failed'
          // throw e
      }
    }
  }

  async add ( pid = req('pid') ) {
    try {

      // Collect the product details from the datebase from the provided pid
      this._products.push
        ( await this._db.getItem( pid ) )

    } catch (e) {

      switch (e.message) {
        // Errors such as item unavailable or not found should be caught here
        case 'Item unavailable':
        // Interpreter error message if no item is found
        // Would be replace by an error handling system
        case "Cannot destructure property `pid` of 'undefined' or 'null'":
          return null
        default:
          throw e
      }
    }
  }

  remove ( pid ) {

    const basketItemIndex = this
      ._products
      .findIndex
        ( item => item.pid === pid )

    if ( basketItemIndex === undefined )
      // throw new Error()
      return

    this._products = this
      ._products
      .filter
        ( ( _, idx ) => !( idx === basketItemIndex ))
  }

  // See knapsack problem (NP Complete)
  // NOTE - Could expand on the to pack the remaining items
  pack () {
    // Sorts the products by delivery time in descending order
    const tmpArray = [ ...this._products ]
      .sort
        ( ( { delivery: a }, { delivery: b } ) => a - b )

    return deliveryWeightPack(tmpArray, this._limit)
  }

  async save () {
    try {
      await this._db.setBasket( this )
    } catch (e) {
      // Error handling here
      throw e
    }
  }
}
