import { runTests, test } from "https://deno.land/std/testing/mod.ts"

import {
  assert,
  assertEquals,
  assertThrows,
  assertThrowsAsync
} from "https://deno.land/std/testing/asserts.ts";

import Basket from './basket.mjs'

import MOCK_DB from '../mocks/database.mjs'

import { MOCK_BASKET_SESSION } from '../mocks/session.mjs'

// false &&
test({
  name: 'Basket - returns "Basket" object',

  async fn () {
    const sessionBasket = new Basket(MOCK_DB, MOCK_BASKET_SESSION)

    await sessionBasket.state

    assert( sessionBasket instanceof Object )

    assert( sessionBasket instanceof Basket )
  }
})

// false &&
test({
  name: 'Basket - throws error for missing db',

  async fn () {
    assertThrowsAsync(async () => {
      await sessionBasket.state

      new Basket()
    })
  }
})

// false &&
test({
  name: 'Basket - collects existing basket products',

  async fn () {
    const existingBasket = await MOCK_DB.getBasketItems()

    const sessionBasket = new Basket(MOCK_DB, MOCK_BASKET_SESSION)

    await sessionBasket.state

    assert
      ( sessionBasket
          .products
          .every
            ( ({ pid }) =>
                existingBasket.includes(pid)
            )
      )
  }
})

// false &&
test({
  name: 'Basket - Adding 0 items throws error',

  async fn () {
    const sessionBasket = await new Basket(MOCK_DB, MOCK_BASKET_SESSION)

    assertThrows(sessionBasket.add(''))
  }
})

// false &&
test({
  name: 'Basket - Test valid pid',

  async fn () {
    const sessionBasket = new Basket(MOCK_DB, MOCK_BASKET_SESSION)

    await sessionBasket.state

    sessionBasket.add('Item1')
  }
})

// false &&
test({
  name: 'Basket - Invalid pid throws error',

  async fn () {
    const sessionBasket = new Basket(MOCK_DB, MOCK_BASKET_SESSION)

    await sessionBasket.state

    assertThrows(sessionBasket.add('asdf'))
  }
})

// false &&
test({
  name: 'Basket - Add method adds a product to the basket',

  async fn () {
    const sessionBasket = new Basket(MOCK_DB, MOCK_BASKET_SESSION)

    await sessionBasket.state

    await sessionBasket.add('Item1')

    const itemOnes = sessionBasket
      .products
      .reduce
        ( ( counter, { pid } ) => 
            pid === 'Item1' ? counter + 1 : counter
        , 0
        ) 

    assertEquals( itemOnes, 2 )
  }
})

// false &&
test({
  name: 'Basket - Remove method removes a product to the basket',

  async fn () {
    const sessionBasket = new Basket(MOCK_DB, MOCK_BASKET_SESSION)

    await sessionBasket.state

    sessionBasket.remove('Item1')

    const itemOnes = sessionBasket
      .products
      .reduce
        ( ( counter, { pid } ) => 
            pid === 'Item1' ? counter + 1 : counter
        , 0
        ) 

    assertEquals( itemOnes, 0 )
  }
})

false &&
test({
  name: 'Basket - pack sorts items in delivery time ascending order',

  async fn () {
    const sessionBasket = new Basket(MOCK_DB, MOCK_BASKET_SESSION)

    await sessionBasket.state

    const packedBasket = sessionBasket.pack()

    assertEquals(1, 1)
  }
})

// false &&
test({
  name: 'Basket - pack sorts items in delivery time ascending order',

  async fn () {
    const sessionBasket = new Basket(MOCK_DB, MOCK_BASKET_SESSION)

    await sessionBasket.state

    const packedBasket = sessionBasket.pack()

    assertEquals(1, 1)
  }
})

runTests()
