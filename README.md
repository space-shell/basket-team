Basket Team
===

Introduction
===
This repository contains the code that handles the basket API code. The project is built on top of the Serverless framework and is made to be deployed as an AWS Lambda function.

For more information on the Serverless framework follow this [Link](https://serverless.com/)

Requirements
===

The following applications are requied to build the services
 - Node = 10 (Matching AWS's current Node version)
 - NPM >= 6
 - Deno (To run unit tests)

The following software is optional
  - entr

Usage
===

To initialise the project first run the following command to install the necessary Node packages:

```shell
  $ npm install
```

Once this step is complete and the Node packages have been installed the following commands are available to use:

To deploy the services to your configured AWS account:

```shell
  $ npm run deploy
```

To invoke functions locally use the following command, the mock JSON data is stored in the `mocks` folder:

```shell
  $ npm run serverless -- invoke local -f <function name> -p <path to mock JSON file>
```

With Deno installed ([Instructions on installing deno](https://deno.land)) run the following command to initialise unit tests:

```shell
 $ npm run test
```

With `entr` installed you can watch for file changes and run test as following:

```
 $ ls libs/* | entr npm run test
```

Overview
===

The basket API logic is located in the `services` folder and the Serverless framework searches for functions in the `services/basket.mjs` file.

Each API endpoint creates a basket instance from the Basket class located in the `libs/basket.mjs`.

The basket class handles the connection between the API endpoint and the Products database this also includes methods to manipulate the basket contents such as adding or removing products.

The Basket class requires a database object (which should be expanded into a full dependency injection model) and an optional session ID to restore existing basket information

Upon instantiation the Basket class sets a `state` property to a promise that resolves to a string description of the baskets status e.g: `new` for a new basket, `restored` for an existing basket.

Development
===

The current system of creating a new basket endpoint is as follows:
  - Create a new module file in the `services` folder that exports an extension of the `Basket` class
  - Have the constructor perform any basket operations and return the API contents (via async IIFE)
  - Import the extended class into the `basket.mjs` file and add export a function that maps to the `services.yml` file

Further Development
===

This project would work well using DynamoDB as a database system to fit with both the serverless framework and the existing AWS integration. At current the project is using the CI pipeline tools that come as part of GitLabs services, at current the pipeline is simple enough to migrate over to a separated CI solution.

Currently an API call retrieves a any previous basket information from a database system and then any associated products before making any basket manipulations. A system that hasn't been developed as of yet is one where multiple API calls create a queue of events, this would protect against race conditions and ensure inventory integrity.
