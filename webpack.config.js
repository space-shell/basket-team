const slsw = require('serverless-webpack')

module.exports = {
  mode: 'none',
  entry: slsw.lib.entries,
  target: 'node',
  module: {
    rules: [
      {
        test: /\.mjs$/,
        loader: 'babel-loader',
        exclude: /node_modules/
      }
    ]
  }
}

