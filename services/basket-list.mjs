import Basket from '../libs/basket.mjs'

export default class BacketList extends Basket {
  constructor
    ( { db
      , session
      , weightLimit
      }
    ) {
      super ( db, session, weightLimit )

      return ( async () => {
        await this.state

        return {
          statusCode: 200,
          body: JSON.stringify(this.products)
        }

      })()
    }
} 

