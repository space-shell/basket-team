import Basket from '../libs/basket.mjs'

export default class BasketAdd extends Basket {
  constructor
    ( { db
      , session
      , weightLimit
      , pid 
      }
    ) {
      super ( db, session, weightLimit )

      return ( async () => {
        await this.state

        try {
          await this.add( pid )

          await this.save()

          return {
            statusCode: 200,
            body: `${pid} Added to the basket`
          }
        } catch (e) {

          switch (e.message) {
            default:
              return {
                statusCode: 500,
                body: `${pid} Not added to the basket`
              }
          }
        }

      })()
    }
} 

