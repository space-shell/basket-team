import Basket from '../libs/basket.mjs'

export default class BasketRemove extends Basket {
  constructor
    ( { db
      , session
      , weightLimit
      , pid 
      }
    ) {
      super ( db, session, weightLimit )

      return ( async () => {
        await this.state

        this.remove( pid )

        await this.save()

        return {
          statusCode: 200,
          body: `${pid} Removed from the basket`
        }

      })()
    }
} 

