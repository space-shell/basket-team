import Basket from '../libs/basket.mjs'

export default class BasketClear extends Basket {
  constructor
    ( { db
      , session
      , weightLimit
      , pid 
      }
    ) {
      super ( db, session, weightLimit )

      return ( async () => {
        await this.state

        this
          .products
          .forEach( ({ pid }) => { this.remove( pid ) } )

        await this.save()

        return {
          statusCode: 200,
          body: `Current basket cleared`
        }

      })()
    }
} 


