import BasketAdd from './basket-add.mjs'
import BasketRemove from './basket-remove.mjs'
import BasketClear from './basket-clear.mjs'
import BasketList from './basket-list.mjs'
import BasketPack from './basket-pack.mjs'

// Get the user session information from information from external service eg: Cognito
// Could also be retieved from the client headers
import { MOCK_BASKET_SESSION } from '../mocks/session.mjs'

// An example service that interfaces with the database eg: dynamoDB
import MOCK_DB from '../mocks/database.mjs'

const BASKET_PARAMS =
  { db: MOCK_DB 
  , session: MOCK_BASKET_SESSION
  }

export const add = async ({ pid }) =>
  await new BasketAdd
    ( { ...BASKET_PARAMS
      , pid
      }
    )

export const remove = async ({ pid }) =>
  await new BasketRemove
    ( { ...BASKET_PARAMS
      , pid
      }
    )

export const clear = async ({ pid }) =>
  await new BasketClear
    ( { ...BASKET_PARAMS
      , pid
      }
    )

export const list = async () =>
  await new BasketList
    ( { ...BASKET_PARAMS
      }
    )

export const pack = async () =>
  await new BasketPack
    ( { ...BASKET_PARAMS
      , weightLimit: 10
      }
    )

