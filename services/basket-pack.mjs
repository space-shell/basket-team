import Basket from '../libs/basket.mjs'

export default class BasketPack extends Basket {
  constructor
    ( { db
      , session
      , weightLimit
      }
    ) {
      super ( db, session, weightLimit )

      return ( async () => {
        await this.state

        const basketPacked = this.pack()

        // NOTE - Get use case for a potentially more logical calculation
        const deliveryDaysTotal = basketPacked
          .reduce
            ( ( a, { delivery: b } ) =>
              a + b
            , 0
            )

        return {
          statusCode: 200,
          body: JSON.stringify({
            basketPacked,
            deliveryDaysTotal
          })
        }

      })()
    }
} 




