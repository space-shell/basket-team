// Pseudo code to represent general database queries

// This service would return 'Product' object
export default {
  async getItem (pid) {
    return [
      {
        pid: 'Item1',
        weight: 1,
        delivery: 4
      }, {
        pid: 'Item2',
        weight: 8,
        delivery: 1
      }, {
        pid: 'Item3',
        weight: 7,
        delivery: 2
      }, {
        pid: 'Item4',
        weight: 4,
        delivery: 10
      }, {
        pid: 'Item5',
        weight: 3,
        delivery: 3
      }, {
        pid: 'Item6',
        weight: 2,
        delivery: 5
      }
    ].find( item => item.pid === pid ) 
  },

  // Would take a session id as an input and either return an existing associated basket or generate a new one
  async getBasketItems () {
    return [
      'Item1',
      'Item2',
      'Item3',
      'Item4',
      'Item5',
      'Item6'
    ]
  },

  // Stores the session basket for use accross multiple services
  // Currently Logs the basket for inspection
  async setBasket ( basket ) {
    console.log( basket )

    return true
  }
}

